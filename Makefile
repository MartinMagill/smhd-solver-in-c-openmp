
# Compiler and options
# Need gnu99 standard to use popen
# Links FFTW and Math
CC = gcc
CFLAGS = -std=gnu99
LFLAGS = -lfftw3 -lm
OPENMP = -fopenmp

###

.PHONY: clean

default: spectralsmhd.exe

spectralsmhd.exe: ./obj/spectralsmhd.o ./obj/constants.o ./obj/meshes.o ./obj/variables.o ./obj/ICs.o
	$(CC) $(CFLAGS) $(OPENMP) -o ./bin/$@ ./obj/* $(LFLAGS)


./obj/spectralsmhd.o: ./src/spectralsmhd.c ./src/constants.h ./src/meshes.h ./src/variables.h
	$(CC) $(CFLAGS) $(OPENMP) -c $(@:obj/%.o=src/%.c) -o $@ $(LFLAGS)


./obj/constants.o: ./src/constants.c ./src/constants.h
	$(CC) $(CFLAGS) $(OPENMP) -c $(@:obj/%.o=src/%.c) -o $@ $(LFLAGS)

./obj/variables.o: ./src/variables.c ./src/variables.h
	$(CC) $(CFLAGS) $(OPENMP) -c $(@:obj/%.o=src/%.c) -o $@ $(LFLAGS)


./obj/ICs.o: ./src/ICs.c ./src/ICs.h ./src/constants.h ./src/meshes.h
	$(CC) $(CFLAGS) $(OPENMP) -c $(@:obj/%.o=src/%.c) -o $@ $(LFLAGS)

./obj/meshes.o: ./src/meshes.c ./src/meshes.h ./src/constants.h ./src/variables.h ./src/ICs.h
	$(CC) $(CFLAGS) $(OPENMP) -c $(@:obj/%.o=src/%.c) -o $@ $(LFLAGS)

clean:
	rm -f ./obj/* ./bin/*

