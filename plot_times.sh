#!/bin/bash


rm tests/*_case*.dat tests/*.png

for casenum in {1..3}
do
   if [ $casenum == 1 ]
   then
      nextcase="/Case 2/"
   fi
   if [ $casenum == 2 ]
   then
      nextcase="/Case 3/"
   fi
   if [ $casenum == 3 ]
   then
      nextcase="$"
   fi

   for size in {3..8}
   do

      sed -n "/Case $casenum/,$nextcase p" tests/matlab_times.dat | egrep "^$size " | awk -v size=$size '{ total += $2 } END { print size " " total/NR }' >> tests/matlab_case"$casenum".dat
      sed -n "/Case $casenum/,$nextcase p" tests/serial_times.dat | egrep "^$size " | awk -v size=$size '{ total += $2 } END { print size " " total/NR }' >> tests/serial_case"$casenum".dat
      sed -n "/Case $casenum/,$nextcase p" tests/2cores_times.dat | egrep "^$size " | awk -v size=$size '{ total += $2 } END { print size " " total/NR }' >> tests/2cores_case"$casenum".dat
      sed -n "/Case $casenum/,$nextcase p" tests/4cores_times.dat | egrep "^$size " | awk -v size=$size '{ total += $2 } END { print size " " total/NR }' >> tests/4cores_case"$casenum".dat
      sed -n "/Case $casenum/,$nextcase p" tests/8cores_times.dat | egrep "^$size " | awk -v size=$size '{ total += $2 } END { print size " " total/NR }' >> tests/8cores_case"$casenum".dat
      sed -n "/Case $casenum/,$nextcase p" tests/new8cores_times.dat | egrep "^$size " | awk -v size=$size '{ total += $2 } END { print size " " total/NR }' >> tests/new8cores_case"$casenum".dat

   done

   size=9
   sed -n "/Case $casenum/,$nextcase p" tests/serial_times.dat | egrep "^$size " | awk -v size=$size '{ total += $2 } END { print size " " total/NR }' >> tests/serial_case"$casenum".dat
   sed -n "/Case $casenum/,$nextcase p" tests/2cores_times.dat | egrep "^$size " | awk -v size=$size '{ total += $2 } END { print size " " total/NR }' >> tests/2cores_case"$casenum".dat
   sed -n "/Case $casenum/,$nextcase p" tests/4cores_times.dat | egrep "^$size " | awk -v size=$size '{ total += $2 } END { print size " " total/NR }' >> tests/4cores_case"$casenum".dat
   sed -n "/Case $casenum/,$nextcase p" tests/8cores_times.dat | egrep "^$size " | awk -v size=$size '{ total += $2 } END { print size " " total/NR }' >> tests/8cores_case"$casenum".dat
   sed -n "/Case $casenum/,$nextcase p" tests/new8cores_times.dat | egrep "^$size " | awk -v size=$size '{ total += $2 } END { print size " " total/NR }' >> tests/new8cores_case"$casenum".dat
   

   gnuplot << EOF
set lmargin at screen 0.15
set rmargin at screen 0.9
set bmargin at screen 0.2
set tmargin at screen 0.9

set xlabel 'log2(Ny)'
set ylabel 'Runtime in Seconds'

set xrange [2:10]
set logscale y
set yrange [0.02:250]
unset key

set terminal png
set output "tests/case$casenum.png"

set multiplot
plot "tests/matlab_case$casenum.dat" using 1:2 lt 1 lw 2 with lines
unset border
unset xtics
unset ytics
unset xlabel
unset ylabel

plot "tests/serial_case$casenum.dat" using 1:2 lt 2 lw 2 with lines
plot "tests/2cores_case$casenum.dat" using 1:2 lt 3 lw 2 with lines
plot "tests/4cores_case$casenum.dat" using 1:2 lt 4 lw 2 with lines
plot "tests/8cores_case$casenum.dat" using 1:2 lt 5 lw 2 with lines
plot "tests/new8cores_case$casenum.dat" using 1:2 lt 6 lw 2 with lines

set key top left
plot 1000 title 'Matlab', \
     1000 title 'C: Serial', \
     1000 title 'C: 2 Threads', \
     1000 title 'C: 4 Threads', \
     1000 title 'C: 8 Threads', \
     1000 title 'new C: 8 Threads'
unset multiplot

EOF


done
