from glob import glob
import os
import numpy as np
import matplotlib.pyplot as plt
my_dpi=96


blocksize = 4096

fig = plt.figure()
fig.set_size_inches(8,8)
ax = plt.Axes(fig, [0., 0., 1., 1.])
ax.set_axis_off()
fig.add_axes(ax)

#fnames = glob('data/*')

#fnames = glob('data/12*_vort.dat')
#artist = ax.imshow(np.ndarray((blocksize,blocksize),dtype=float), vmin=-1e-2, vmax=1e-2, aspect='auto')

#fnames = glob('data/12*_e.dat')
#artist = ax.imshow(np.ndarray((blocksize,blocksize),dtype=float), vmin=-1, vmax=1, aspect='auto')

#fnames = glob('data/12*_b1.dat')
#artist = ax.imshow(np.ndarray((blocksize,blocksize),dtype=float), vmin=100-20, vmax=100+20, aspect='auto')

#fnames = glob('data/12*_b2.dat')
#artist = ax.imshow(np.ndarray((blocksize,blocksize),dtype=float), vmin=-10, vmax=10, aspect='auto')

#fnames = glob('data/12*_u1.dat')
#artist = ax.imshow(np.ndarray((blocksize,blocksize),dtype=float), vmin=-1, vmax=1, aspect='auto')

fnames = glob('data/12*_u2.dat')
artist = ax.imshow(np.ndarray((blocksize,blocksize),dtype=float), vmin=-1, vmax=1, aspect='auto')

A = np.ndarray((blocksize,blocksize),dtype=float)

for i in range(len(fnames)):

    print("Plotting %s" % fnames[i])

    with open(fnames[i],'r') as fin:

	basename = os.path.basename(fnames[i])
	for j, line in enumerate(fin):

		k = j % blocksize

		A[k,:] = np.array(line.strip().split("\t"))

		if (j % blocksize) == (blocksize-1):
			plotnum = int(j/blocksize)+1
			print("\t Plot %d" % plotnum)
			artist.set_data(A)
			plt.draw()
			plt.savefig("plots/%s_plot%d.png" % (basename,plotnum), dpi=5*my_dpi, bbox_inches=0)
