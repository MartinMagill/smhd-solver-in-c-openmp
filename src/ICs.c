#include "ICs.h"


double e_0(struct Meshes* Mesh, int ii, int jj) {
// Initial height configuration

   double H = Mesh->H;

   double Nx = Mesh->Nx;
   double Ny = Mesh->Ny;
   double dx = Mesh->dx;
   double dy = Mesh->dy;
   double Lx = Mesh->Lx;
   double Ly = Mesh->Ly;

   // Make height a gaussian centered at the middle
//   return  0.05 *H * exp(- ( pow(dy*(jj-Ny/2.),2.) ) / pow(Lx/8.,2.) );
   return 0.;

}

double ut_0(struct Meshes* Mesh, int ii, int jj) {
// U_theta for Gaussian vorticity

   double c0 = Mesh->c0;

   double Nx = Mesh->Nx;
   double Ny = Mesh->Ny;
   double dx = Mesh->dx;
   double dy = Mesh->dy;
   double Lx = Mesh->Lx;
   double Ly = Mesh->Ly;

   double x = dx*(ii-Nx/2.);
   double y = dy*(jj-Ny/2.);

   double r = sqrt( x*x + y*y );

   // Gaussian vortex, G = , R = Ly/10
   double G = Mesh->G;
   double R = Ly/10;
   return G * (r * r / R / R) * exp(- r*r / (2*pow(R,2.)) );
}

double u1_0(struct Meshes* Mesh, int ii, int jj) {
// Initial horizontal velocity configuration

   double Nx = Mesh->Nx;
   double Ny = Mesh->Ny;
   double dx = Mesh->dx;
   double dy = Mesh->dy;

   double x = dx*(ii-Nx/2.);
   double y = dy*(jj-Ny/2.);

   double r = sqrt( x*x + y*y );

   return ut_0(Mesh,ii,jj) * y / (r+0.0001);

}

double u2_0(struct Meshes* Mesh, int ii, int jj) {
// Initial vertical velocity configuration

   double Nx = Mesh->Nx;
   double Ny = Mesh->Ny;
   double dx = Mesh->dx;
   double dy = Mesh->dy;

   double x = dx*(ii-Nx/2.);
   double y = dy*(jj-Ny/2.);

   double r = sqrt( x*x + y*y );

   return ut_0(Mesh,ii,jj) * (-x) / (r+0.0001);

}

double b1_0(struct Meshes* Mesh, int ii, int jj) {
// Initial horizontal magnetic field configuration
return Mesh->B0;
}

double b2_0(struct Meshes* Mesh, int ii, int jj) {
// Initial vertical magnetic field configuration
return 0.;
}
