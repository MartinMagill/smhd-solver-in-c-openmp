#ifndef _ICS_H_
#define _ICS_H_

#include <math.h>
#include "constants.h"
#include "meshes.h"

double  e_0 (struct Meshes*, int, int);
double u1_0 (struct Meshes*, int, int);
double u2_0 (struct Meshes*, int, int);
double b1_0 (struct Meshes*, int, int);
double b2_0 (struct Meshes*, int, int);


#endif
