#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include "constants.h"
#include "meshes.h"



int main (int argc, char* argv[]) {



   //// //// //// //// //// ////

   //// Simulation Parameters ////

   if (argc!=10) {
      printf("\n\n");
      printf("\t%s log2(Ny) (Nx/Ny) latitude Lx(degs) {1(Rad),2(Ovr)} alpha circulation B_flag plotting_flag \n\n",argv[0]);
      printf("\t\twhere plotting_flag controls live plotting as follows:\n\n");
      printf("\t\t0:\tno plot\n");
      printf("\t\t1:\theight\n");
      printf("\t\t2:\tx velocity\n");
      printf("\t\t3:\ty velocity\n");
      printf("\t\t4:\tx magnetic field\n");
      printf("\t\t5:\ty magnetic field\n");
      printf("\t\t6:\tkinetic energy\n");
      printf("\t\t7:\tmagnetic leakage\n");
      printf("\t\t8:\tvorticity\n");
      printf("\t\t-n:\tno saved output\n");
      printf("\n\n");
      exit(1);
   }

   // Mesh Parameters
   int Ny = pow(2,(int) strtol(argv[1],(char **) NULL, 10));            // Command Line Argument 1:   log2(Ny)
   int AspectRatio = (int) strtol(argv[2],(char **) NULL, 10);          // Command Line Argument 2:   Nx/Ny
   int Nx = AspectRatio*Ny;

   // Latitude 
   double latitude = strtod(argv[3],(char **) NULL);                    // Command Line Argument 3:   latitude

   // Dimensions
   double L0 = circumf_Tach();
   double Lx = strtod(argv[4],(char **) NULL) * L0/360;                 // Command Line Argument 4:   Lx
   double Ly = Lx / AspectRatio;
   double omega = omega_Tach();
   double B0 = B_Tach();
   double H;
   if ((int) strtol(argv[5],(char **) NULL, 10) == 1) H = H_Tach_Rad;   // Command Line Argument 5:   Rad (1) or Ovr (2)
   else H = H_Tach_Ovr;
   double g = strtod(argv[6],(char **) NULL) * g_Tach();                // Command Line Argument 6:   Buoyancy Factor (Alpha)
   double G = strtod(argv[7],(char **) NULL);                           // Command Line Argument 7:   Initial Circulation G

   // Magnetic field on/off
   B0 = strtod(argv[8],(char **) NULL) * B0;                            // Command Line Argument 8:   Magnetic Field on/off Flag
   

   // Plotting
   int plotting = (int) strtol(argv[9],(char **) NULL, 10);             // Command Line Argument 9:   Plotting Flag

   // Case Name
   char casename[256];
   sprintf(casename,"%s_%s_%s_%s_%s_%s_%s_%s_%s",argv[1],argv[2],argv[3],argv[4],argv[5],argv[6],argv[7],argv[8],argv[9]);

   //// End of Simulation Parameters ////

   //// //// //// //// //// ////

   //// Start Main Program ////

   printf("\nWelcome to Martin Magill's Spectral SMHD Solver!\n\n\n");


   // Make grids
   printf("Making meshes... ");
   struct Meshes* Mesh = malloc(sizeof(struct Meshes));
   InitializeMesh(Mesh, Nx, Ny, Lx, Ly, latitude, H, omega, g, B0, G, plotting, casename);
   printf("Meshes made.\n");

   // Set up time stepping
   double cfl = fmin(Mesh->dx,Mesh->dy) / (Mesh->c0);
   double dt = 0.01 * cfl;
   double Ti = 0;
   double Tf = fmin(Mesh->Lx, Mesh->Ly) / (Mesh->c0);    // Time for gravity waves to reach a boundary
   //   Tf = Tf/10; // Mag waves move faster

   // Initial conditions and one step of Euler
   printf("Setting ICs... ");
   FillTi(Mesh); // Fill the first timestep with the ICs
   printf("ICs set.\n");

   printf("Doing initial timestep... ");
   EulerStep(Mesh,dt); // Fill the second timestep using the first
   printf("Initial timestep done.\n");
   PrintMeshToFile(Mesh,0);

   // Primary iterations using Leap Frog
   printf("Entering main loop:\n\n");
   double t = Ti;
   int n_step, output_flag, status;
   int plot_freq = floor( (Tf/dt)/900 );  // Make at least 90 saves

   while (t < Tf) {

      n_step = (int) round(t/dt);
      output_flag = (n_step % plot_freq) == 0;

      if (output_flag) printf("\tDoing timestep %d... ", (int) round(t/dt));
      status = LeapFrog(Mesh,dt);
      if (output_flag) printf("Done.\t%2d%% Complete\n",(int) round(100*t/Tf));

      // Check for errors
      if (!status) {
         printf("NaNs Detected!\n");
         break;
      }

      // Print
      if (output_flag) PrintMeshToFile(Mesh,ceil(t/3600));

      t = t + dt;

   }

   printf("\nFinished main loop.\n");
   if (plotting != 0) {
      printf("Press enter to continue... ");
      getchar();
      printf("Done.\n");
   }

   //// End Main Program ////

   printf("De-allocating memory... ");
   FreeMesh(Mesh);
   free(Mesh);
   printf("Done.\n");
   printf("Goodbye!\n\n");
 
   return 0;

}
