#include "meshes.h"
#include <string.h>


//// //// //// //// //// ////


// Physical constants


//// //// //// //// //// ////


void InitializeMesh(struct Meshes* Mesh, int Nx, int Ny, double Lx, double Ly, double latitude, double H, double omega, double gR, double B0, double G, int plotting, char* casename) {

   //// Constants

   Mesh->Nx = Nx;
   Mesh->Ny = Ny;
   Mesh->Lx = Lx;
   Mesh->Ly = Ly;

   Mesh->latitude = latitude;
   Mesh->H = H;
   Mesh->gR = gR;
   Mesh->omega = omega;
   Mesh->B0 = B0;
   Mesh->G = G;

   Mesh->f = 2*omega*sin(latitude*pi/180.);
   Mesh->c0 = sqrt(gR*H);


   //// Spatial Mesh

   // Intervals (for periodic BCs)
   double dx = (2. / Nx) * Lx;
   double dy = (2. / Ny) * Ly;
   Mesh->dx = dx;
   Mesh->dy = dy;

   //// Fourier Mesh

   // Intervals
   double dk = pi/Lx;
   double dl = pi/Ly;
   Mesh->dk = dk;
   Mesh->dl = dl;
   Mesh->k = malloc(Nx * sizeof(double));
   Mesh->l = malloc(Ny * sizeof(double));
   Mesh->k_filter = malloc(Nx * sizeof(double));
   Mesh->l_filter = malloc(Ny * sizeof(double));

   // Wavenumbers are stored [0, 1, 2, ..., N/2 - 1, 0, 1 - N/2, ..., -1]
   for (int ii=0; ii<Nx/2; ii++) {
      Mesh->k[ii] = ii * dk;
      Mesh->k[ii+(Nx/2)] = ( ii - (Nx/2) ) * dk;
   }
   Mesh->k[(Nx/2)] = 0;
   for (int ii=0; ii<Ny/2; ii++) {
      Mesh->l[ii] = ii * dl;
      Mesh->l[ii+(Ny/2)] = ( ii - (Ny/2) ) * dl;
   }
   Mesh->l[(Ny/2)] = (double) 0;


   // Filter
   for (int ii=0; ii<Nx; ii++) {
      if (fabs(Mesh->k[ii]) < 0.65 * dk*Nx/2) { Mesh->k_filter[ii] = 1; }
      else { Mesh->k_filter[ii] = exp( log(1e-8)*pow( (fabs(Mesh->k[ii])-(0.65*dk*Nx/2))/(0.35*dk*Nx/2), 8. ) ); }
   }
   for (int ii=0; ii<Ny; ii++) {
      if (fabs(Mesh->l[ii]) < 0.65 * dl*Ny/2) { Mesh->l_filter[ii] = 1; }
      else { Mesh->l_filter[ii] = exp( log(1e-8)*pow( (fabs(Mesh->l[ii])-(0.65*dl*Ny/2))/(0.35*dl*Ny/2), 8. ) ); }
   }
   Mesh->k_filter[Nx/2] = 0;
   Mesh->l_filter[Ny/2] = 0;

   //// Fluid Variables
   Mesh->e_i = malloc(sizeof(struct Variables));
   Mesh->e_j = malloc(sizeof(struct Variables));
   Mesh->e_k = malloc(sizeof(struct Variables));
   Mesh->u1_i = malloc(sizeof(struct Variables));
   Mesh->u1_j = malloc(sizeof(struct Variables));
   Mesh->u1_k = malloc(sizeof(struct Variables));
   Mesh->u2_i = malloc(sizeof(struct Variables));
   Mesh->u2_j = malloc(sizeof(struct Variables));
   Mesh->u2_k = malloc(sizeof(struct Variables));
   Mesh->b1_i = malloc(sizeof(struct Variables));
   Mesh->b1_j = malloc(sizeof(struct Variables));
   Mesh->b1_k = malloc(sizeof(struct Variables));
   Mesh->b2_i = malloc(sizeof(struct Variables));
   Mesh->b2_j = malloc(sizeof(struct Variables));
   Mesh->b2_k = malloc(sizeof(struct Variables));

   InitializeVariable(Mesh->e_i,Nx,Ny);
   InitializeVariable(Mesh->e_j,Nx,Ny);
   InitializeVariable(Mesh->e_k,Nx,Ny);
   InitializeVariable(Mesh->u1_i,Nx,Ny);
   InitializeVariable(Mesh->u1_j,Nx,Ny);
   InitializeVariable(Mesh->u1_k,Nx,Ny);
   InitializeVariable(Mesh->u2_i,Nx,Ny);
   InitializeVariable(Mesh->u2_j,Nx,Ny);
   InitializeVariable(Mesh->u2_k,Nx,Ny);
   InitializeVariable(Mesh->b1_i,Nx,Ny);
   InitializeVariable(Mesh->b1_j,Nx,Ny);
   InitializeVariable(Mesh->b1_k,Nx,Ny);
   InitializeVariable(Mesh->b2_i,Nx,Ny);
   InitializeVariable(Mesh->b2_j,Nx,Ny);
   InitializeVariable(Mesh->b2_k,Nx,Ny);


   // Variables for nonlinear height evolution terms
   Mesh->NLT1_i = malloc(sizeof(struct Variables));
   Mesh->NLT1_j = malloc(sizeof(struct Variables));
   Mesh->NLT1_k = malloc(sizeof(struct Variables));
   Mesh->NLT2_i = malloc(sizeof(struct Variables));
   Mesh->NLT2_j = malloc(sizeof(struct Variables));
   Mesh->NLT2_k = malloc(sizeof(struct Variables));

   InitializeVariable(Mesh->NLT1_i,Nx,Ny);
   InitializeVariable(Mesh->NLT1_j,Nx,Ny);
   InitializeVariable(Mesh->NLT1_k,Nx,Ny);
   InitializeVariable(Mesh->NLT2_i,Nx,Ny);
   InitializeVariable(Mesh->NLT2_j,Nx,Ny);
   InitializeVariable(Mesh->NLT2_k,Nx,Ny);


   // Prep output files
   // Only the names are set up here
   // One file per timestep is better for post-processing
   if (plotting>=0) {
      Mesh->casename = casename;
      Mesh->timestep = 0;
      char filename[512];
      sprintf(filename,"data/%s_e_%.5d.dat",Mesh->casename,Mesh->timestep);
      strcpy(Mesh->fe, filename);
      sprintf(filename,"data/%s_u1_%.5d.dat",Mesh->casename,Mesh->timestep);
      strcpy(Mesh->fu1, filename);
      sprintf(filename,"data/%s_u2_%.5d.dat",Mesh->casename,Mesh->timestep);
      strcpy(Mesh->fu2, filename);
      sprintf(filename,"data/%s_b1_%.5d.dat",Mesh->casename,Mesh->timestep);
      strcpy(Mesh->fb1, filename);
      sprintf(filename,"data/%s_b2_%.5d.dat",Mesh->casename,Mesh->timestep);
      strcpy(Mesh->fb2, filename);
      sprintf(filename,"data/%s_vort_%.5d.dat",Mesh->casename,Mesh->timestep);
      strcpy(Mesh->fvort, filename);
   }

   // Plotting
   Mesh->gnuplot_pipe = popen("gnuplot -persist > /dev/null 2>&1","w");
   fprintf(Mesh->gnuplot_pipe,"set terminal x11 1\n");
   Mesh->plotting = plotting;

}

void FreeMesh(struct Meshes* Mesh){


   fprintf(Mesh->gnuplot_pipe,"set terminal x11 1 close\n");
   fclose(Mesh->gnuplot_pipe);

   FreeVariable(Mesh->NLT1_i);
   FreeVariable(Mesh->NLT1_j);
   FreeVariable(Mesh->NLT1_k);
   FreeVariable(Mesh->NLT2_i);
   FreeVariable(Mesh->NLT2_j);
   FreeVariable(Mesh->NLT2_k);

   free(Mesh->NLT1_i);
   free(Mesh->NLT1_j);
   free(Mesh->NLT1_k);
   free(Mesh->NLT2_i);
   free(Mesh->NLT2_j);
   free(Mesh->NLT2_k);

   FreeVariable(Mesh->e_i);
   FreeVariable(Mesh->e_j);
   FreeVariable(Mesh->e_k);
   FreeVariable(Mesh->u1_i);
   FreeVariable(Mesh->u1_j);
   FreeVariable(Mesh->u1_k);
   FreeVariable(Mesh->u2_i);
   FreeVariable(Mesh->u2_j);
   FreeVariable(Mesh->u2_k);
   FreeVariable(Mesh->b1_i);
   FreeVariable(Mesh->b1_j);
   FreeVariable(Mesh->b1_k);
   FreeVariable(Mesh->b2_i);
   FreeVariable(Mesh->b2_j);
   FreeVariable(Mesh->b2_k);

   free(Mesh->e_i);
   free(Mesh->e_j);
   free(Mesh->e_k);
   free(Mesh->u1_i);
   free(Mesh->u1_j);
   free(Mesh->u1_k);
   free(Mesh->u2_i);
   free(Mesh->u2_j);
   free(Mesh->u2_k);
   free(Mesh->b1_i);
   free(Mesh->b1_j);
   free(Mesh->b1_k);
   free(Mesh->b2_i);
   free(Mesh->b2_j);
   free(Mesh->b2_k);

   free(Mesh->k);
   free(Mesh->l);
   free(Mesh->k_filter);
   free(Mesh->l_filter);

}


//// //// //// //// //// ////


void FillTi(struct Meshes* Mesh) {

   int kk, ii;
   #pragma omp parallel for private(kk,ii)
   for (int jj=0; jj<Mesh->Ny; jj++) {
   for (ii=0; ii<Mesh->Nx; ii++) {
      kk = ii+jj*Mesh->Nx;
      Mesh->e_i->g[kk] = e_0(Mesh,ii,jj);
      Mesh->u1_i->g[kk] = u1_0(Mesh,ii,jj);
      Mesh->u2_i->g[kk] = u2_0(Mesh,ii,jj);
      Mesh->b1_i->g[kk] = b1_0(Mesh,ii,jj);
      Mesh->b2_i->g[kk] = b2_0(Mesh,ii,jj);
      Mesh->NLT1_i->g[kk] = (Mesh->H + e_0(Mesh,ii,jj)) * u1_0(Mesh,ii,jj);
      Mesh->NLT2_i->g[kk] = (Mesh->H + e_0(Mesh,ii,jj)) * u2_0(Mesh,ii,jj);

   }}

}


//// //// //// //// //// ////


void EulerStep(struct Meshes* Mesh, double dt) {
// Fill time j using time i

   // Update derivatives at time i
   #pragma omp parallel 
   {
      #pragma omp sections
      {
      #pragma omp section
      UpdateDerivs(Mesh->e_i,Mesh->k,Mesh->l,Mesh->k_filter,Mesh->l_filter);
      #pragma omp section
      UpdateDerivs(Mesh->u1_i,Mesh->k,Mesh->l,Mesh->k_filter,Mesh->l_filter);
      #pragma omp section
      UpdateDerivs(Mesh->u2_i,Mesh->k,Mesh->l,Mesh->k_filter,Mesh->l_filter);
      #pragma omp section
      UpdateDerivs(Mesh->b1_i,Mesh->k,Mesh->l,Mesh->k_filter,Mesh->l_filter);
      #pragma omp section
      UpdateDerivs(Mesh->b2_i,Mesh->k,Mesh->l,Mesh->k_filter,Mesh->l_filter);
      #pragma omp section
      UpdateDerivs(Mesh->NLT1_i,Mesh->k,Mesh->l,Mesh->k_filter,Mesh->l_filter);
      #pragma omp section
      UpdateDerivs(Mesh->NLT2_i,Mesh->k,Mesh->l,Mesh->k_filter,Mesh->l_filter);
      }
   

      // Step each variable field independently of the others
      int kk, ii;
      #pragma omp for private(kk,ii)
      for (int jj=0; jj<Mesh->Ny; jj++) {
      for (ii=0; ii<Mesh->Nx; ii++) {
         kk = ii+jj*Mesh->Nx;
   

         // Update physical quantities
         Mesh->e_j->g[kk] = Mesh->e_i->g[kk] 
                                - dt * ( + Mesh->NLT1_i->gx[kk]                      // x Contribution
                                         + Mesh->NLT2_i->gy[kk] );                   // y Contribution


         Mesh->u1_j->g[kk] = Mesh->u1_i->g[kk] 
                                - dt *(- Mesh->f * Mesh->u2_i->g[kk]                 // Rotation
                                       + Mesh->u1_i->g[kk] * Mesh->u1_i->gx[kk]      // Advection in x
                                       + Mesh->u2_i->g[kk] * Mesh->u1_i->gy[kk]      // Advection in y
                                       - Mesh->b1_i->g[kk] * Mesh->b1_i->gx[kk]      // Induction in x
                                       - Mesh->b2_i->g[kk] * Mesh->b1_i->gy[kk]      // Induction in y
                                       + Mesh->gR * Mesh->e_i->gx[kk] );             // Gravitation

         Mesh->u2_j->g[kk] = Mesh->u2_i->g[kk] 
                                - dt *(+ Mesh->f * Mesh->u1_i->g[kk]                 // Rotation
                                       + Mesh->u1_i->g[kk] * Mesh->u2_i->gx[kk]      // Advection in x
                                       + Mesh->u2_i->g[kk] * Mesh->u2_i->gy[kk]      // Advection in y
                                       - Mesh->b1_i->g[kk] * Mesh->b2_i->gx[kk]      // Induction in x
                                       - Mesh->b2_i->g[kk] * Mesh->b2_i->gy[kk]      // Induction in y
                                       + Mesh->gR * Mesh->e_i->gy[kk] );             // Gravitation
   
         Mesh->b1_j->g[kk] = Mesh->b1_i->g[kk] 
                                - dt *(+ Mesh->u1_i->g[kk] * Mesh->b1_i->gx[kk]      // Advection in x
                                       + Mesh->u2_i->g[kk] * Mesh->b1_i->gy[kk]      // Advection in y
                                       - Mesh->b1_i->g[kk] * Mesh->u1_i->gx[kk]      // Induction in x
                                       - Mesh->b2_i->g[kk] * Mesh->u1_i->gy[kk]);    // Induction in y

         Mesh->b2_j->g[kk] = Mesh->b2_i->g[kk] 
                                - dt *(+ Mesh->u1_i->g[kk] * Mesh->b2_i->gx[kk]      // Advection in x
                                       + Mesh->u2_i->g[kk] * Mesh->b2_i->gy[kk]      // Advection in y
                                       - Mesh->b1_i->g[kk] * Mesh->u2_i->gx[kk]      // Induction in x
                                       - Mesh->b2_i->g[kk] * Mesh->u2_i->gy[kk]);    // Induction in y

         // Update NLTs
         Mesh->NLT1_j->g[kk] = ( Mesh->H + Mesh->e_j->g[kk] ) * Mesh->u1_j->g[kk];
         Mesh->NLT2_j->g[kk] = ( Mesh->H + Mesh->e_j->g[kk] ) * Mesh->u2_j->g[kk];
   
      }}   
   }

}



int LeapFrog(struct Meshes* Mesh, double dt) {
   // Fill time k with times i and j
   
   // Update derivatives at time j
   int status = 1;
   #pragma omp parallel 
   {

      #pragma omp sections
      {
      #pragma omp section
      UpdateDerivs(Mesh->e_j,Mesh->k,Mesh->l,Mesh->k_filter,Mesh->l_filter);
      #pragma omp section
      UpdateDerivs(Mesh->u1_j,Mesh->k,Mesh->l,Mesh->k_filter,Mesh->l_filter);
      #pragma omp section
      UpdateDerivs(Mesh->u2_j,Mesh->k,Mesh->l,Mesh->k_filter,Mesh->l_filter);
      #pragma omp section
      UpdateDerivs(Mesh->b1_j,Mesh->k,Mesh->l,Mesh->k_filter,Mesh->l_filter);
      #pragma omp section
      UpdateDerivs(Mesh->b2_j,Mesh->k,Mesh->l,Mesh->k_filter,Mesh->l_filter);
      #pragma omp section
      UpdateDerivs(Mesh->NLT1_j,Mesh->k,Mesh->l,Mesh->k_filter,Mesh->l_filter);
      #pragma omp section
      UpdateDerivs(Mesh->NLT2_j,Mesh->k,Mesh->l,Mesh->k_filter,Mesh->l_filter);
      }

      // Step each variable field independently of the others
      int kk, ii;
      #pragma omp for private(kk,ii)
      for (int jj=0; jj<Mesh->Ny; jj++) {
      for (ii=0; ii<Mesh->Nx; ii++) {
         kk = ii+jj*Mesh->Nx;
   

         // Update physical quantities
         Mesh->e_k->g[kk] = Mesh->e_i->g[kk] 
                              - 2*dt * ( + Mesh->NLT1_j->gx[kk]                      // x Contribution
                                         + Mesh->NLT2_j->gy[kk] );                   // y Contribution
   
   
         Mesh->u1_k->g[kk] = Mesh->u1_i->g[kk] 
                              - 2*dt *(- Mesh->f * Mesh->u2_j->g[kk]                 // Rotation
                                       + Mesh->u1_j->g[kk] * Mesh->u1_j->gx[kk]      // Advection in x
                                       + Mesh->u2_j->g[kk] * Mesh->u1_j->gy[kk]      // Advection in y
                                       - Mesh->b1_j->g[kk] * Mesh->b1_j->gx[kk]      // Induction in x
                                       - Mesh->b2_j->g[kk] * Mesh->b1_j->gy[kk]      // Induction in y
                                       + Mesh->gR * Mesh->e_j->gx[kk] );             // Gravitation
   
         Mesh->u2_k->g[kk] = Mesh->u2_i->g[kk] 
                              - 2*dt *(+ Mesh->f * Mesh->u1_j->g[kk]                 // Rotation
                                       + Mesh->u1_j->g[kk] * Mesh->u2_j->gx[kk]      // Advection in x
                                       + Mesh->u2_j->g[kk] * Mesh->u2_j->gy[kk]      // Advection in y
                                       - Mesh->b1_j->g[kk] * Mesh->b2_j->gx[kk]      // Induction in x
                                       - Mesh->b2_j->g[kk] * Mesh->b2_j->gy[kk]      // Induction in y
                                       + Mesh->gR * Mesh->e_j->gy[kk] );             // Gravitation
      
         Mesh->b1_k->g[kk] = Mesh->b1_i->g[kk] 
                              - 2*dt *(+ Mesh->u1_j->g[kk] * Mesh->b1_j->gx[kk]      // Advection in x
                                       + Mesh->u2_j->g[kk] * Mesh->b1_j->gy[kk]      // Advection in y
                                       - Mesh->b1_j->g[kk] * Mesh->u1_j->gx[kk]      // Induction in x
                                       - Mesh->b2_j->g[kk] * Mesh->u1_j->gy[kk]);    // Induction in y
   
         Mesh->b2_k->g[kk] = Mesh->b2_i->g[kk] 
                              - 2*dt *(+ Mesh->u1_j->g[kk] * Mesh->b2_j->gx[kk]      // Advection in x
                                       + Mesh->u2_j->g[kk] * Mesh->b2_j->gy[kk]      // Advection in y
                                       - Mesh->b1_j->g[kk] * Mesh->u2_j->gx[kk]      // Induction in x
                                       - Mesh->b2_j->g[kk] * Mesh->u2_j->gy[kk]);    // Induction in y
   
         // Update NLTs 
         Mesh->NLT1_k->g[kk] = ( Mesh->H + Mesh->e_k->g[kk] ) * Mesh->u1_k->g[kk];
         Mesh->NLT2_k->g[kk] = ( Mesh->H + Mesh->e_k->g[kk] ) * Mesh->u2_k->g[kk];
   
         // Step time
         Mesh->e_i->g[kk] = Mesh->e_j->g[kk];
         Mesh->u1_i->g[kk] = Mesh->u1_j->g[kk];
         Mesh->u2_i->g[kk] = Mesh->u2_j->g[kk];
         Mesh->b1_i->g[kk] = Mesh->b1_j->g[kk];
         Mesh->b2_i->g[kk] = Mesh->b2_j->g[kk];
         Mesh->NLT1_i->g[kk] = Mesh->NLT1_j->g[kk];
         Mesh->NLT2_i->g[kk] = Mesh->NLT2_j->g[kk];
      
         Mesh->e_j->g[kk] = Mesh->e_k->g[kk];
         Mesh->u1_j->g[kk] = Mesh->u1_k->g[kk];
         Mesh->u2_j->g[kk] = Mesh->u2_k->g[kk];
         Mesh->b1_j->g[kk] = Mesh->b1_k->g[kk];
         Mesh->b2_j->g[kk] = Mesh->b2_k->g[kk];
         Mesh->NLT1_j->g[kk] = Mesh->NLT1_k->g[kk];
         Mesh->NLT2_j->g[kk] = Mesh->NLT2_k->g[kk];
   
      }}

      #pragma omp for reduction(&& : status)
      for (int ii=0; ii<Mesh->Nx*Mesh->Ny; ii++){
         status = status && !isnan(Mesh->e_j->g[ii])
                         && !isnan(Mesh->u1_j->g[ii])
                         && !isnan(Mesh->u2_j->g[ii])
                         && !isnan(Mesh->b1_j->g[ii])
                         && !isnan(Mesh->b2_j->g[ii]);
      }
      
   }
   return status;
}


void PrintMeshToFile(struct Meshes* Mesh, int n_hours) {
// Print timestep j into each file

   // Print contiguously so that gnuplot can process it
   if (Mesh->plotting >= 0) {
   FILE* fe = fopen(Mesh->fe,"w");
   FILE* fu1 = fopen(Mesh->fu1,"w");
   FILE* fu2 = fopen(Mesh->fu2,"w");
   FILE* fb1 = fopen(Mesh->fb1,"w");
   FILE* fb2 = fopen(Mesh->fb2,"w");
   FILE* fvort = fopen(Mesh->fvort,"w");
   for (int jj=0; jj<Mesh->Ny; jj++) {
      for (int ii=0; ii<Mesh->Nx; ii++) {
         fprintf(fe,"%.10e\t", Mesh->e_j->g[ii+jj*Mesh->Nx] / (0.1*Mesh->H) );
         fprintf(fu1,"%.10e\t", Mesh->u1_j->g[ii+jj*Mesh->Nx] / Mesh->c0);
         fprintf(fu2,"%.10e\t", Mesh->u2_j->g[ii+jj*Mesh->Nx] / Mesh->c0);
         fprintf(fb1,"%.10e\t", Mesh->b1_j->g[ii+jj*Mesh->Nx] / (0.01*Mesh->B0) );
         fprintf(fb2,"%.10e\t", Mesh->b2_j->g[ii+jj*Mesh->Nx] / (0.01*Mesh->B0) );
         fprintf(fvort,"%.10e\t", Mesh->u2_j->gx[ii+jj*Mesh->Nx]
                                      -Mesh->u1_j->gy[ii+jj*Mesh->Nx] / (Mesh->c0*Mesh->f/4) );
      }
      fprintf(fe,"\n");
      fprintf(fu1,"\n");
      fprintf(fu2,"\n");
      fprintf(fb1,"\n");
      fprintf(fb2,"\n");
      fprintf(fvort,"\n");
   }
 
   // Close this timestep's files
   fclose(fe);
   fclose(fu1);
   fclose(fu2);
   fclose(fb1);
   fclose(fb2);
   fclose(fvort);

   // Update file names
   Mesh->timestep++;
   char filename[512];
   sprintf(filename,"data/%s_e_%.5d.dat",Mesh->casename,Mesh->timestep);
   strcpy(Mesh->fe, filename);
   sprintf(filename,"data/%s_u1_%.5d.dat",Mesh->casename,Mesh->timestep);
   strcpy(Mesh->fu1, filename);
   sprintf(filename,"data/%s_u2_%.5d.dat",Mesh->casename,Mesh->timestep);
   strcpy(Mesh->fu2, filename);
   sprintf(filename,"data/%s_b1_%.5d.dat",Mesh->casename,Mesh->timestep);
   strcpy(Mesh->fb1, filename);
   sprintf(filename,"data/%s_b2_%.5d.dat",Mesh->casename,Mesh->timestep);
   strcpy(Mesh->fb2, filename);
   sprintf(filename,"data/%s_vort_%.5d.dat",Mesh->casename,Mesh->timestep);
   strcpy(Mesh->fvort, filename);

   }

   // Plot height field live
   if (Mesh->plotting != 0) {
   char title[80];
   sprintf(title,"Time: %8d hours",n_hours);
   fprintf(Mesh->gnuplot_pipe, "set title \"%s\"\n", title);
   fprintf(Mesh->gnuplot_pipe, "set size ratio %.1f\n", (double) Mesh->Ny/Mesh->Nx);
   fprintf(Mesh->gnuplot_pipe, "set view map\n");
   fprintf(Mesh->gnuplot_pipe, "set key off\n");
   fprintf(Mesh->gnuplot_pipe, "set cbrange [-1:1]\n");
   fprintf(Mesh->gnuplot_pipe, "splot '-' matrix with image\n");
   // Flip row-column to plot right
   int kk;
   for (int jj=0; jj<Mesh->Ny; jj++) {
      for (int ii=0; ii<Mesh->Nx; ii++) {
         kk = ii+jj*Mesh->Nx;

         switch(abs(Mesh->plotting))
         {
         case 1 :
            fprintf(Mesh->gnuplot_pipe,"%f ", Mesh->e_j->g[kk] / (0.01*Mesh->H) );
            break;
         case 2 :
            fprintf(Mesh->gnuplot_pipe,"%f ", Mesh->u1_j->g[kk] / Mesh->c0);
            break;
         case 3 :
            fprintf(Mesh->gnuplot_pipe,"%f ", Mesh->u2_j->g[kk] / Mesh->c0);
            break;
         case 4 :
            fprintf(Mesh->gnuplot_pipe,"%f ", (Mesh->b1_j->g[kk] - Mesh->B0) / (0.1*Mesh->B0) );
            break;
         case 5 :
            fprintf(Mesh->gnuplot_pipe,"%f ", Mesh->b2_j->g[kk] / (0.1*Mesh->B0) );
            break;
         case 6 :
            fprintf(Mesh->gnuplot_pipe,"%f ", sqrt(pow(Mesh->u1_j->g[kk],2)
                                                  +pow(Mesh->u2_j->g[kk],2)) / Mesh->c0);
            break;
         case 7 :  // div(height * B) , computed by product rule, divided by |h*B|/(Lx/8)
            fprintf(Mesh->gnuplot_pipe,"%f ",
                                    (  Mesh->e_j->g[kk] * Mesh->b1_j->gx[kk]
                                     + Mesh->e_j->gx[kk] * Mesh->b1_j->g[kk]
                                     + Mesh->e_j->gy[kk] * Mesh->b2_j->g[kk]
                                     + Mesh->e_j->g[kk] * Mesh->b2_j->gy[kk])

                                  / (     (fabs(Mesh->e_j->g[kk]) + Mesh->H)
                                         * sqrt( pow(Mesh->b1_j->g[kk],2.)
                                                +pow(Mesh->b2_j->g[kk],2.))  // Getting 0/0 problems. Find an equivalent ratio that goes to 1 instead of 0
                                         / (Mesh->Lx/8.)                    ) );
            break;
         case 8 :
            fprintf(Mesh->gnuplot_pipe,"%f ",  Mesh->u2_j->gx[ii+jj*Mesh->Nx]
                                             - Mesh->u1_j->gy[ii+jj*Mesh->Nx] / (10*Mesh->c0/Mesh->Ly) );


         }
      }
      fprintf(Mesh->gnuplot_pipe,"\n");
   }
   fprintf(Mesh->gnuplot_pipe, "e\n");
   fprintf(Mesh->gnuplot_pipe, "e\n");
   
   fflush(Mesh->gnuplot_pipe);
   }
}





