#ifndef _CONSTANTS_H_
#define _CONSTANTS_H_

#include <math.h>

   double pi;

   double Radius_Sun;
   double g_Sun;
   
   double omegaA_Sun;
   double omegaB_Sun;
   double omegaC_Sun;
   double omega_Sun (double);

   double Radius_Tach();
   double g_Tach();
   double circumf_Tach();
   double rho_Tach;
   double realB_Tach;
   double B_Tach();

   double omega_Tach();

   double H_Tach_Rad;
   double gAlpha_Tach_Rad;
   double gR_Tach_Rad();

   double H_Tach_Ovr;
   double gAlpha_Tach_Ovr;
   double gR_Tach_Ovr();









#endif
